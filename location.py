import os
from os.path import join
from tqdm.auto import tqdm
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

import math
import geopy.distance

from datetime import datetime, timedelta


bases = [
    {"name": "Rega-Center", "lat": 47.457990, "lon": 8.572991},
    {"name": "Rega_1", "lat": 47.395876, "lon": 8.637898},
    {"name": "Rega_2", "lat": 47.605896, "lon": 7.523330},
    {"name": "Rega_3", "lat": 46.909619, "lon": 7.504851},
    {"name": "Rega_4", "lat": 46.547337, "lon": 6.618223},
    {"name": "Rega_5", "lat": 46.912776, "lon": 9.551239},
    {"name": "Rega_6", "lat": 46.163171, "lon": 8.881034},
    {"name": "Rega_7", "lat": 47.405521, "lon": 9.290079},
    {"name": "Rega_8", "lat": 46.834069, "lon": 8.638286},
    {"name": "Rega_9", "lat": 46.530427, "lon": 9.878443},
    {"name": "Rega_10", "lat": 46.670054, "lon": 7.876414},
    {"name": "Rega_12", "lat": 47.078150, "lon": 9.066240},
    {"name": "Rega_14", "lat": 46.554903, "lon": 7.379185},
    {"name": "Rega_15", "lat": 46.233339, "lon": 6.096148},
    {"name": "Trainingsbasis Grenchen", "lat": 47.181693, "lon": 7.411192}
]

positions = None
positions_per_unit = {}


def init():
    global positions
    # Load helicopter positions
    header = ["altitude", "avlid", "bearing", "carid", "cdts", "deviceflags", "deviceinfo", 
          "ext_unitattr", "gpstime", "latitude", "loc_x", "loc_y", "location", 
          "longitude", "num_1", "result", "source", "speed", "status", "tr_status",
          "unid", "unitstatus"]
    positions = pd.read_csv("data/new/ger_avl_log_rega.csv", delimiter=';', names=header)
    positions['gpstime'] = pd.to_datetime(positions['gpstime'])
    positions.sort_values('gpstime', inplace=True)
    for unit in positions['unid'].unique():
       unit_positions = positions[positions['unid'] == unit] 
       positions_per_unit[unit] = unit_positions


init()


def get_tailnumber(callsign):
    callsign = callsign.upper()
    if callsign in ["REGA_3", "HB-ZQH"]:
        return "HB-ZQH"
    elif callsign in ["REGA_1", "HB-ZQG"]:
        return "HB-ZQG"
    raise

def get_location_before(start_time: datetime, callsign: str):
    # print(start_time, callsign)  -- carid - tailsign, unid - callsign
    x = positions[positions['unid'] == callsign]#get_tailnumber(callsign)]
    # Select positions not older than one week
    x = x[
        (x['gpstime'] >= (start_time - timedelta(hours=24*7)))
        & 
        (x['gpstime'] <= start_time)
    ]
    # Select only positions before start time
    # x = x[x['gpstime'] <= start_time]
    if x.shape[0] <= 0:
        raise Exception("Default position")
        # TODO: return default base position
    else:
        # Select the latest position
        return 0, 0, 0
        idx = x['gpstime'].idxmax()
        return x.loc[idx, 'gpstime'], (x.loc[idx, 'latitude'], x.loc[idx, 'longitude'])


def get_location_after(start_time: datetime, callsign: str):
    x = positions[positions['unid'] == callsign]# == get_tailnumber(callsign)]
    # Select only positions after start time
    x = x[x['gpstime'] >= start_time]
    # Select positions not later than 30 minutes
    x = x[x['gpstime'] <= (start_time + timedelta(minutes=30))]
    if x.shape[0] <= 0:
        raise Exception("Default position")
        # TODO: return default base position
    else:
        # Select the oldest position
        idx = x['gpstime'].idxmin()
        return x.loc[idx, 'gpstime'], (x.loc[idx, 'latitude'], x.loc[idx, 'longitude'])


def is_at_base(rega_base, lat, lng):
    base_lat, base_lng = bases[rega_base]['lat'], bases[rega_base]['lon']
    distance = geopy.distance.distance((base_lat, base_lng), (lat, lng)).km
    return distance < 5


def get_last_known_time(mission):
    if mission['Abschlusszeit']:
        return mission['Abschlusszeit']
    elif mission['Abflug_Einsatzort']:
        return mission['Abflug_Einsatzort']
    elif mission['Ankunft_Einsatzort']:
        return mission['Ankunft_Einsatzort']
    return mission['liftoff_time'] - pd.Timedelta("1s")  # will be dropped


def filter_positions_for_mission(mission, which='t1+'):
    if which not in ['t1', 't1+', 'all']: raise Exception("Not implemented")

    unit = mission['Einsatzmittel']
    if unit not in positions_per_unit:
        print("Unknown unit", unit)
        return positions[positions['latitude'] == 129048240]  # TODO: raise or whatever
    
    cols = ['carid', 'unid', 'latitude', 'longitude', 'gpstime', 'bearing']
    mission_positions = positions_per_unit[unit]
    mission_positions = mission_positions[cols]
    # mission_positions = mission_positions[mission_positions['unid'] == mission['Einsatzmittel']]
    mission_positions = mission_positions[mission_positions['gpstime'] >= mission['liftoff_time']]
    if which == 't1':
        # delta = timedelta(hours=1)
        mission_positions = mission_positions[mission_positions['gpstime'] <= mission['Ankunft_Einsatzort']]
    elif which == 't1+':
        mission_positions = mission_positions[mission_positions['gpstime'] <= mission['Abflug_Einsatzort']]
    elif which == 'all':
        end_time = get_last_known_time(mission)
        mission_positions = mission_positions[mission_positions['gpstime'] <= end_time]
    return mission_positions


def get_location_differences(mission, mission_positions=None):
    if mission_positions is None:
        mission_positions = filter_positions_for_mission(mission)
    distances = []
    last_pos = None

    for idx, row in mission_positions.iterrows(): 
        curr_pos = (row['latitude'], row['longitude']) 
        if last_pos is not None: 
            dist = geopy.distance.distance(last_pos, curr_pos).km
            distances.append(dist) 
        last_pos = curr_pos
    
    return distances


def project_mercator(latitude, longitude):
    """Project the longitude / latitude coords to the unit square.

    :param longitude: In degrees, between -180 and 180
    :param latitude: In degrees, between -85 and 85

    :return: Coordinates `(x,y)` in the "Web Mercator" projection, normalised
      to be in the range [0,1].
    Forked from tilemapbase.
    """
    xtile = (longitude + 180.0) / 360.0
    lat_rad = np.radians(latitude)
    ytile = (1.0 - np.log(np.tan(lat_rad) + (1 / np.cos(lat_rad))) / math.pi) / 2.0
    return (xtile, ytile)


def get_point_nearby(positions, landing_point, threshold_km=2, outside=False):
    import geopy.distance
    # pos = location.filter_positions_for_mission(mission, which='all')
    for idx, row in positions.iterrows():
        lat, lng = row['latitude'], row['longitude']
        if (geopy.distance.distance(landing_point, (lat, lng)).km < threshold_km) != outside:
            return (idx, row['gpstime'], lat, lng)
    return None


def get_arrival_at_patient(mission, positions):
    landing_point = (mission['WGS84_Latitude'], mission['WGS84_Longitude'])
    return get_point_nearby(positions, landing_point)


def get_departure_at_patient(mission, positions, arrival_time):
    landing_point = (mission['WGS84_Latitude'], mission['WGS84_Longitude'])
    positions = positions[positions['gpstime'] > arrival_time]
    return get_point_nearby(positions, landing_point, outside=True)
