import os
import logging
from os.path import join
from tqdm.auto import tqdm
import pandas as pd
import matplotlib.pyplot as plt
import numpy as np

import math
import geopy.distance

from datetime import datetime, timedelta

import location
import multiprocessing


def load_missions():
    missions = pd.read_csv("data/new/Rega_ELS_Heli_Mission_Export_New.csv")
    missions = missions.set_index("Einsatznummer_ELS")

    good_types = open("data/important_keywords.csv").read().split(",")
    missions = missions[missions['Einsatzstichwort'].isin(good_types)]

    missions.drop("Einsatzmittel", axis=1, inplace=True)
    mission_ids = pd.read_csv("data/new/Mission_IDs.csv")
    mission_ids = mission_ids.set_index("Einsatznummer_ELS")
    missions = missions.join(mission_ids, how='inner')
    missions = missions[~missions['Einsatzmittel'].isna()]
    missions['Alarmeingang'] = pd.to_datetime(missions['Alarmeingang'])
    return missions


def add_liftoff_positions(missions):
    liftoff_times, start_lat, start_lng = [], [], []
    total_cnt, ok_cnt = 0, 0
    for idx, row in tqdm(missions.iterrows()):
        start_t = row['Alarmeingang']
        total_cnt += 1
        try:
            callsign = row['Einsatzmittel']
            ref_t, (lat, lng) = location.get_location_after(start_t, callsign)
            liftoff_times.append(ref_t)
            start_lat.append(lat)
            start_lng.append(lng)
            ok_cnt += 1
        except:
            # logging.exception("Foo")
            liftoff_times.append(None)
            start_lat.append(None)
            start_lng.append(None)
    missions['liftoff_time'] = liftoff_times
    missions['liftoff_lat'] = start_lat
    missions['liftoff_lng'] = start_lng
    print(f"Added {ok_cnt} / {total_cnt} liftoff times and positions")
    return missions


def add_liftoff_positions_parallel(missions):
    # liftoff_times, start_lat, start_lng = [], [], []
    num_cores = multiprocessing.cpu_count()# - 1
    df_split = np.array_split(missions, num_cores)
    pool = multiprocessing.Pool(num_cores)
    df = pd.concat(pool.map(add_liftoff_positions, df_split))
    return df


def add_times_patient(missions):
    times_t1a, times_t1b, times_t1c, lats, lngs = [], [], [], [], []
    for idx, row in tqdm(missions.iterrows()):
        # time_start = row['Helikopter_alarmiert'] if not pd.isna(row['Helikopter_alarmiert']) else row['Alarmeingang']
        time_start = row['liftoff_time']
        # time_arrival = row['Ankunft_Einsatzort']
        # time_t1 = time_arrival - time_start
        # times_t1.append(time_t1)

        positions = location.filter_positions_for_mission(mission=row, which='all')
        # estimate landing @ patient location
        landing_patient = location.get_arrival_at_patient(mission=row, positions=positions)
        if landing_patient is not None:
            (idx, gpstime, latitude, longitude) = landing_patient
            time_t1a = gpstime - time_start
            times_t1a.append(time_t1a); lats.append(latitude); lngs.append(longitude)
            times_t1b.append(row['Ankunft_Einsatzort'] - gpstime)

            # estimate takeoff @ patient location
            to_patient = location.get_departure_at_patient(mission=row, positions=positions, arrival_time=gpstime)
            if to_patient is not None:
                (idx, gpstime_2, latitude, longitude) = to_patient
                time_t1c = gpstime_2 - gpstime
                times_t1c.append(time_t1c)
            else:
                times_t1c.append(None)

        else:
            times_t1a.append(None); lats.append(np.nan); lngs.append(np.nan)
            times_t1b.append(None); times_t1c.append(None)
    
    missions['time_patient'] = times_t1a
    missions['time_until_landing'] = times_t1b
    missions['patient_arrival_lat'] = lats
    missions['patient_arrival_lng'] = lngs
    missions['time_on_site'] = times_t1c
    return missions


def add_times_patient_from_crew(missions):
    times_t1 = []
    for idx, row in tqdm(missions.iterrows()):
        # time_start = row['Helikopter_alarmiert'] if not pd.isna(row['Helikopter_alarmiert']) else row['Alarmeingang']
        time_start = row['liftoff_time']
        time_arrival = row['Ankunft_Einsatzort']
        # TODO TODO: get the destination times in the same way as liftoff times

        time_t1 = time_arrival - time_start
        # time_t1 /= np.timedelta64(1,'m')
        times_t1.append(time_t1)
    missions['time_patient'] = times_t1
    return missions


def apply_parallel(df, fn):
    num_cores = multiprocessing.cpu_count()# - 1
    df_split = np.array_split(df, num_cores)
    pool = multiprocessing.Pool(num_cores)
    df = pd.concat(pool.map(fn, df_split))
    return df


def add_patient_arrival_positions_parallel(missions):
    num_cores = multiprocessing.cpu_count()# - 1
    df_split = np.array_split(missions, num_cores)
    pool = multiprocessing.Pool(num_cores)
    df = pd.concat(pool.map(add_times_patient, df_split))
    return df



def add_elevations(missions):
    missions = missions.dropna(subset=['Ankunft_Einsatzort'])
    elevations = pd.read_csv("results_elevation.csv")
    assert missions.shape[0] == elevations.shape[0], (missions.shape[0], elevations.shape[0])
    elevations = elevations.set_index("Einsatznummer_ELS")
    elevations = elevations[['elevation_patient', 'elevation_liftoff']]
    # return missions.join(elevations, how='left')  # can't be done (duplicate ids)
    missions['elevation_patient'] = elevations['elevation_patient'].copy()
    missions['elevation_liftoff'] = elevations['elevation_liftoff'].copy()
    return missions


def add_elevations_and_weather(missions):
    missions = missions.dropna(subset=['Ankunft_Einsatzort'])
    elevations = pd.read_csv("additional_features.csv")
    assert missions.shape[0] == elevations.shape[0], (missions.shape[0], elevations.shape[0])
    elevations = elevations.set_index("Einsatznummer_ELS")
    # return missions.join(elevations, how='left')  # can't be done (duplicate ids)
    elevations = elevations[['elevation_patient', 'elevation_liftoff', 'temperature', 'humidity', 'rain', 'wind']]
    missions['elevation_patient'] = elevations['elevation_patient'].copy()
    missions['elevation_liftoff'] = elevations['elevation_liftoff'].copy()
    missions['temperature'] = elevations['temperature'].copy()
    missions['humidity'] = elevations['humidity'].copy()
    missions['rain'] = elevations['rain'].copy()
    missions['wind'] = elevations['wind'].copy()
    return missions


def add_weather(missions):
    weather_data = load_weather_data()

    weather_cols = np.zeros((missions.shape[0], 4))
    for i in range(weather_cols.shape[0]):
        time = missions['liftoff_time'][i]
        location = (missions['liftoff_lat'][i], missions['liftoff_lng'][i])

        if np.isnan(location[0]) or np.isnan(location[1]):
            continue

        weather_cols[i, :] = get_weather(weather_data, time, location)

    np.savetxt("weather_data.csv", weather_cols, delimiter=",")

    return missions


def load_weather_data():
    weather_files = ['EBBE_Zaeziwil.csv',
                     'EBBO_Spiez.csv',
                     'EBBS_Therwil.csv',
                     'EBGE_Tannay.csv',
                     'EBGR_Zizers.csv',
                     'EBLS_Pully.csv',
                     'EBMO_Siebnen.csv',
                     'EBSG_Herisau.csv',
                     'EBTI_Cugnasco.csv',
                     'EBZH_Lindau.csv',
                     ]
    dfs = [pd.read_csv('data/' + f, encoding='iso-8859-1', sep=';', low_memory=False) for f in weather_files]
    relevant_cols = [0, 4, 7, 10]   # date/time, humidity, precipitation, windspeed

    data = []
    for df in dfs:
        t = np.array(df)[2:,0]

        tmp = ', '.join(np.array(df)[2:,1]).replace('x', 'nan').replace('?', 'nan')
        tmp_float = np.fromstring(tmp, dtype=np.float, sep=', ')

        hum = ', '.join(np.array(df)[2:,4]).replace('x', 'nan').replace('?', 'nan')
        hum_float = np.fromstring(hum, dtype=np.float, sep=', ')

        rain = ', '.join(np.array(df)[2:,7]).replace('x', 'nan').replace('?', 'nan')
        rain_float = np.fromstring(rain, dtype=np.float, sep=', ')

        wind = ', '.join(np.array(df)[2:,10]).replace('x', 'nan').replace('?', 'nan')
        wind_float = np.fromstring(wind, dtype=np.float, sep=', ')

        data.append(np.c_[t, tmp_float, hum_float, rain_float, wind_float])

    return data


def get_nearest_weather_station(lat, lon):
    location_coordinates = [
        (46.898486, 7.662931),   # Zaeziwil
        (46.684412, 7.676955),   # Spiez
        (47.494274, 7.555505),   # Therwil
        (46.308294, 6.177783),   # Tannay
        (46.943668, 9.555943),   # Zizers
        (46.507825, 6.660150),   # Pully
        (47.174007, 8.897644),   # Siebnen
        (47.384247, 9.278395),   # Herisau
        (46.171543, 8.919557),   # Cugnasco
        (47.563284, 9.687767),   # Lindau
    ]
    dists = [geopy.distance.distance((lat, lon), station).km for station in location_coordinates]
    return np.argmin(dists)


# get weather columns for given datetime object and location=(lat, lon) from nearest weather station
def get_weather(weather_data, time, location):
    station = get_nearest_weather_station(*location)

    l = 0
    r = weather_data[station].shape[0] - 1
    while l <= r:
        m = l + (r - l) // 2
        if np.abs(time - datetime.strptime(weather_data[station][m,0], '%d.%m.%Y %H:%M')) < timedelta(hours=1):
            # found it
            return weather_data[station][m, 1:]
        elif time - datetime.strptime(weather_data[station][m,0], '%d.%m.%Y %H:%M') > timedelta(hours=1):
            l = m + 1
        else:
            r = m - 1
    return np.array(['nan', 'nan', 'nan', 'nan'], dtype=float)
