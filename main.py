import os
import numpy as np
import pandas as pd
import location
from missions import load_missions, add_liftoff_positions, \
add_liftoff_positions_parallel, add_times_patient, add_elevations, \
apply_parallel
from visualize import extract_flight_features

from sklearn.metrics import mean_absolute_error, mean_squared_error, \
    explained_variance_score, median_absolute_error, r2_score
from sklearn.linear_model import LinearRegression, Ridge, Lasso
from sklearn.svm import SVR
from sklearn.ensemble import GradientBoostingRegressor, AdaBoostRegressor
from sklearn.model_selection import train_test_split
import sklearn.preprocessing

import matplotlib.pyplot as plt
import geopy.distance


def convert_datetimes(missions):
    missions['Abflug_Einsatzort'] = pd.to_datetime(missions['Abflug_Einsatzort'])
    missions['Ankunft_Einsatzort'] = pd.to_datetime(missions['Ankunft_Einsatzort'])
    missions['Abschlusszeit'] = pd.to_datetime(missions['Abschlusszeit'])
    # These columns might not exist yet
    try:
        missions['liftoff_time'] = pd.to_datetime(missions['liftoff_time'])
    except:
        pass
    try:
        missions['time_patient'] = pd.to_timedelta(missions['time_patient'])
    except:
        pass
    try:
        missions['time_until_landing'] = pd.to_timedelta(missions['time_until_landing'])
    except:
        pass
    try:
        missions['time_on_site'] = pd.to_timedelta(missions['time_on_site'])
    except:
        pass
    return missions


def missions_with_liftoff_positions():
    if os.path.exists("missions_cached.csv"):
        missions = pd.read_csv("missions_cached.csv")
        return convert_datetimes(missions)
    else:
        missions = load_missions()
        print(missions.head())
        print(missions.shape)
        # Need: starting location
        missions = add_liftoff_positions_parallel(missions)
        missions.to_csv("missions_cached.csv")
    return missions


def missions_with_patient_arrival_positions():
    if os.path.exists("missions_cached.1.csv"):
        missions = pd.read_csv("missions_cached.1.csv")
        return convert_datetimes(missions)
    else:
        missions = missions_with_liftoff_positions()
        # print("At first:", missions.shape)
        # missions = missions.dropna(subset=['Abschlusszeit'])
        print("After dropping Abschlusszeit:", missions.shape)
        missions = apply_parallel(missions, add_times_patient)
        missions.to_csv("missions_cached.1.csv")
    
    missions = missions.set_index("Einsatznummer_ELS")
    return missions


def missions_with_trajectory_data():
    if os.path.exists("missions_cached.2.csv"):
        missions = pd.read_csv("missions_cached.2.csv")
        return convert_datetimes(missions)
    else:
        missions = missions_with_patient_arrival_positions()
        missions = extract_flight_features(missions, show_results=False)
        missions.to_csv("missions_cached.2.csv")
    
    missions = missions.set_index("Einsatznummer_ELS")
    return missions


def main():
    missions = missions_with_trajectory_data()

    missions = missions.dropna(subset=['time_patient', 'distance_ratio'])
    print("After adding patient times:", missions.shape)

    # missions = add_elevations(missions)
    # Used to be 7547 valid for "time_patient", now when using trajectories 8085 valid
    missions = missions.dropna(subset=['time_patient'])
    # missions = missions.dropna(subset=['time_patient', 
    # 'elevation_patient', 'elevation_liftoff'])
    missions = missions[missions['time_patient'] < np.timedelta64(6, 'h')]
    missions = missions[missions['time_patient'] > pd.Timedelta('-1us')]
    # Only 4 rows were dropped here

    plt.hist(missions['time_patient'] / np.timedelta64(1, 'm'), bins=100)
    plt.savefig("t1-distribution-new.png")
    plt.show()
    
    print(f"Number of predicted missions: {missions.shape[0]}")

    y_pred = predict_baseline(missions)
    y_true = missions['time_patient'] / np.timedelta64(1,'m')
    accuracy_report(y_pred, y_true, "Baseline (mean)")

    y_pred = predict_regression_abs(missions)
    accuracy_report(y_pred, y_true, "Linear Regression (abs pos)")

    # y_pred = predict_regression_diff(missions)
    # accuracy_report(y_pred, y_true, "Linear Regression (pos diff)")

    # y_pred = predict_regression_diff(missions, use_vehicles=True)
    # accuracy_report(y_pred, y_true, "Linear Regression (pos diff + vehicle)")

    # y_pred = predict_regression_diff(missions, use_vehicles=True, use_trajectory=True)
    # accuracy_report(y_pred, y_true, "Linear Regression (pos diff + vehicle + trajectory)")

    # y_pred = predict_regression_diff(missions, use_vehicles=True, use_trajectory='multiply')
    # accuracy_report(y_pred, y_true, "Linear Regression (pos diff + vehicle + trajectory + trajectory * distance)")

    # y_pred = predict_regression_diff(missions, use_vehicles=True, use_trajectory='multiply', use_season=True)
    # accuracy_report(y_pred, y_true, "Linear Regression (pos diff + vehicle + trajectory + trajectory * distance + time data)")

    print("=================================================================================================================")
    (X_train, y_pred_train, y_true_train), (X_test, y_pred_test, y_true_test) = \
        predict_regression_diff(missions, use_vehicles=False, use_trajectory=False, do_split=True)
    accuracy_report(y_pred_train, y_true_train, "Linear Regression (pos diff) - on training data")
    accuracy_report(y_pred_test, y_true_test, "Linear Regression (pos diff) - on testing data")

    (X_train, y_pred_train, y_true_train), (X_test, y_pred_test, y_true_test) = \
        predict_regression_diff(missions, use_vehicles=True, use_trajectory=True, do_split=True)
    accuracy_report(y_pred_train, y_true_train, "Linear Regression (best features) - on training data", residuals=True)
    accuracy_report(y_pred_test, y_true_test, "Linear Regression (best features) - on testing data", residuals=True)

    # y_pred = predict_regression_diff(missions, use_vehicles=True, use_elevation=True)
    # accuracy_report(y_pred, y_true, "Linear Regression (pos diff + vehicle + elevation)")

    residuals = (y_pred_test - y_true_test)
    print(X_test['distance'].mean())
    plt.hist(X_test['distance'], bins=20)
    plt.show()
    print(X_test[residuals < -20].shape)
    # for i in X_test[residuals < -20].index:
    #     print(i)
    #     try:
    #         from visualize import plot_trajectory
    #         plot_trajectory(missions.loc[i])
    #     except:
    #         pass


def predict_baseline(data):
    # Predicts time to the patient as the sample mean.
    y_pred = (data['time_patient'] / np.timedelta64(1,'m')).mean()
    return [y_pred] * data.shape[0]


def predict_regression_abs(data):
    reg = LinearRegression()
    y = data['time_patient'] / np.timedelta64(1,'m')
    # NOTE: This should NOT work, since we have to use XY differences instead!
    X = data[['liftoff_lat', 'liftoff_lng', 'WGS84_Latitude', 'WGS84_Longitude']]
    reg.fit(X, y)
    return reg.predict(X)

# R^2 0.089, 0.111 without km -> 0.102, 0.124 with km

def predict_regression_diff(data, use_vehicles=False, use_elevation=False, use_trajectory=False, use_season=False, do_split=False):
    reg = GradientBoostingRegressor(loss='huber', subsample=0.7, n_estimators=500) 
    # reg = SVR()
    # reg = Ridge(alpha=10)  #LinearRegression()
    scaler = sklearn.preprocessing.StandardScaler()

    y = data['time_patient'] / np.timedelta64(1,'m')
    # NOTE: This SHOULD work better than the methods above!
    X = data[['liftoff_lat', 'liftoff_lng', 'WGS84_Latitude', 'WGS84_Longitude']]
    lat_delta = (data['liftoff_lat'] - data['WGS84_Latitude']).abs()
    lng_delta = (data['liftoff_lng'] - data['WGS84_Longitude']).abs()
    
    X_new = pd.concat((lat_delta, lng_delta), axis=1)
    # print(data.columns)
    X_new['distance'] = data.apply(
        lambda r: geopy.distance.distance(
            (r['liftoff_lat'], r['liftoff_lng']),
            (r['WGS84_Latitude'], r['WGS84_Longitude'])
        ).km
    , axis = 1 )
    # print(X_new.head())

    if use_vehicles:
        vehicle = pd.get_dummies(data['Einsatzmittel'])
        X_new = pd.concat((X_new, vehicle), axis=1)
    
    if use_elevation:
        height_diff = (data['elevation_patient'] - data['elevation_liftoff'])#.abs()
        X_new['elevation_diff'] = height_diff
        X_new['elevation_patient'] = data['elevation_patient']
    
    if use_trajectory:
        X_new['distance_ratio'] = data['distance_ratio']
        if use_trajectory == 'multiply':
            X_new['distance_x_ratio'] = X_new['distance_ratio'] * X_new['distance']

    if use_season:
        for month in range(1, 12):
            X_new['is_month_%d' % month] = (data['liftoff_time'].dt.month == month)
        for hour in range(0, 23):
            X_new['is_hour_%d' % hour] = (data['liftoff_time'].dt.hour == hour)

    if do_split:
        X_train, X_test, y_train, y_test = train_test_split(X_new, y, test_size=0.2, random_state=42)
        scaler.fit(X_train)
        reg.fit(scaler.transform(X_train), y_train)
        return \
            (X_train, reg.predict(scaler.transform(X_train)), y_train), \
            (X_test, reg.predict(scaler.transform(X_test)), y_test)
    else:
        scaler.fit(X_new)
        reg.fit(scaler.transform(X_new), y)
        return reg.predict(scaler.transform(X_new))


def accuracy_report(y_pred, y_true, name, residuals=False):
    mae, mse = mean_absolute_error(y_pred, y_true), mean_squared_error(y_pred, y_true, squared=False)
    # evar = explained_variance_score(y_true, y_pred)
    r2 = r2_score(y_true, y_pred)
    print(f"Accuracy report for \t {name}")
    print(f"RMSE: {mse:.3f} MAE: {mae:.3f} R^2 (explained var): {r2:.3f}")
    print()
    # print(f"R^2 score (explained var): {r2:.3f}")
    if residuals:
        residuals = (y_pred - y_true)
        plt.scatter(range(len(residuals)), residuals)
        plt.show()


if __name__ == "__main__":
    main()
