var map = L.map('map').setView([46.79, 8.32], 8);

// add helicopter bases to map
var bases = [
    {name: "Rega 1", lat: 47.395876, lon: 8.637898, vehicleWeight: -1.2288906},
    {name: "Rega 2", lat: 47.605896, lon: 7.523330, vehicleWeight: -1.07300319},
    {name: "Rega 3", lat: 46.909619, lon: 7.504851, vehicleWeight: -1.51707714 },
    {name: "Rega 4", lat: 46.547337, lon: 6.618223, vehicleWeight: -1.20581611},
    {name: "Rega 5", lat: 46.912776, lon: 9.551239, vehicleWeight: 0.33669062},
    {name: "Rega 6", lat: 46.163171, lon: 8.881034, vehicleWeight: 0.68802531},
    {name: "Rega 7", lat: 47.405521, lon: 9.290079, vehicleWeight: -1.10553061},
    {name: "Rega 8", lat: 46.834069, lon: 8.638286, vehicleWeight: 1.62407988},
    {name: "Rega 9", lat: 46.530427, lon: 9.878443, vehicleWeight: 0.48542489},
    {name: "Rega 10", lat: 46.670054, lon: 7.876414, vehicleWeight: 0.35597018},
    {name: "Rega 12", lat: 47.078150, lon: 9.066240, vehicleWeight: -0.07708018},
    {name: "Rega 14", lat: 46.554903, lon: 7.379185, vehicleWeight: 0.04101756},
    {name: "Rega 15", lat: 46.233339, lon: 6.096148, vehicleWeight: 1.36202831},
    //{name: "Trainingsbasis Grenchen", lat: 47.181693, lon: 7.411192}
    //{name: "Rega-Center", lat: 47.457990, lon: 8.572991},
];
//Rega17 -> 10
//Rega17 -> 10
// 'Rega_16',  'Rega_17',
//0.01523228  0.10846541

var baseToVehicle = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
var heli_icon = L.icon({
    iconUrl: 'images/heli_icon.png',
    iconSize:     [80, 45], // size of the icon
});
for(var i = 0; i<bases.length; i++) {
    L.marker([bases[i].lat, bases[i].lon], {icon: heli_icon}).bindPopup(bases[i].name).addTo(map);
}


// add map tiles
L.tileLayer('https://tile.osm.ch/switzerland/{z}/{x}/{y}.png', {
    attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors',
    maxZoom: 18
}).addTo(map);


// create different layers for parts of UI
var predictionLayer = L.layerGroup()
var heatMapLayer = L.layerGroup()
var serviceRegionLayer = L.layerGroup()

// checkbox event handler
function togglePrediction(object) {
    if(object.checked) {
        predictionLayer.clearLayers()
        predictionLayer.addTo(map)
    } else {
        predictionLayer.removeFrom(map)
    }
}
function toggleHeat(object) {
    if(object.checked) {
        heatMapLayer.addTo(map)
    } else {
        heatMapLayer.removeFrom(map)
    }
}
function toggleService(object) {
    if(object.checked) {
        serviceRegionLayer.addTo(map)
    } else {
        serviceRegionLayer.removeFrom(map)
    }
}



// --------------------------- PREDICTION LAYER ----------------------------------------------

// Init colors
var gRainbow = null;
var gKnnTree = null;
var gKnnTrees = new Map();

function initColors() {
    import('../libs/rainbowvis.js').then(function(obj) {
        window.gRainbow = new obj.Rainbow();
        window.gRainbow.setNumberRange(10, 25);
        window.gRainbow.setSpectrum('green', 'yellow', 'red');
    });
}

function initKNN() {
    //import('./ml-knn/knn.js').then(function(obj){
    //const { default: KNN } = obj;
    // The following CSV file can be easily prepared in Python as:
    // df = pd.read_csv("../missions_cached.2.csv")
    // x = df[['Einsatzmittel', 'patient_arrival_lat', 'patient_arrival_lng', 'time_patient']]
    // x['time_patient'] = pd.to_timedelta(x['time_patient']) / pd.Timedelta(1, 'm')
    // x['Einsatzmittel'] = x['Einsatzmittel'].apply(lambda i: i.replace('_', ' '))
    // x.dropna().to_csv("./data/for_knn.csv", index=False, header=False)
    //csv_data = Papa.parse("/data/for_knn.csv", {download: false}).data;
    //var knn = new KNN(csv_data, []);
    //});

    var dist = function(a,b) { return distance(a[0], a[1], b[0], b[1]); }
    //import('./kdTree.js').then(function (obj){
    Papa.parse("data/for_knn.csv", {download: true, complete: function (results) {
        csv_data = results.data;
        // console.log("KNN OK");
        csv_data.forEach(function(item) {
            var key = item[0];  // Einsatzmittel
            var point = item.slice(1,4);
            // console.log(point);
            if (!gKnnTrees.has(key)) {
                gKnnTrees.set(key, new kdTree([point], dist, [0,1]));
            } else {
                gKnnTrees.get(key).insert(point);
            }
        });
    }});

    //});
}

function initKNN1() {
    var which = 't1a'
    $.ajax({
        url: "data/heatmap." + which + ".json",
        async: false,
        dataType: 'json',
        success: function(data) {
            //import('./kdTree.js').then(function (obj) {
            var dist = function(a,b) { return distance(a[0], a[1], b[0], b[1]); }
            window.gKnnTree = new kdTree(data, dist, [0, 1]);
            //});
        }
    });
}


initColors();
initKNN();

// select point on map
var selectedPoint = L.marker()
var prevLines = null

function updateMarkerPos(lat, lon) {
    // calculate all distances
    var preds = bases.map((bases, i) => predict(i, lat, lon))
    var physical_preds =  bases.map((bases, i) => physical_model(i, lat, lon))
    var knn_preds = bases.map(function(base, i) {
        try {
            var preds = window.gKnnTrees.get(base.name).nearest([lat, lon], 16, 10);
            var mean = 0.0;
            for (i = 0; i < 16; i++) {
                mean += parseFloat(preds[i][0][2]);
            }
            //[0][0][2];
            return mean / 16;
        } catch(err) {
            // console.log(window.gKnnTrees, base.name);
            return "no data";
        }
    })

    // sort indices w.r.t. preds
    var indices = new Array(preds.length)
    for(var i = 0; i < preds.length; i++) indices[i] = i
    indices = indices.sort(function (a, b) { return preds[a] < preds[b] ? -1 : preds[a] > preds[b] ? 1 : 0; })

    // draw lines to 3 nearest ones
    if(prevLines != null) prevLines.map(l => l.removeFrom(predictionLayer))
    colors = ['green', 'orange', 'red']


    function componentToHex(c) {
        var hex = c.toString(16);
        return hex.length == 1 ? "0" + hex : hex;
    }
    function rgbToHex(r, g, b) {
        return "#" + componentToHex(r) + componentToHex(g) + componentToHex(b);
    }

    function timeToColor(time) {
        if (window.gRainbow != null) {
            return "#" + window.gRainbow.colourAt(time);
        } else {
            t1_avg = 10   // set proper values here
            t1_max = 23.5   // set proper values here
            var r = Math.floor(Math.min(1, time / t1_avg) * 255);
            var g = Math.floor((1 - time / t1_max) * 255);
            return rgbToHex(r, g, 0)
        }
    }

    var lines = indices.slice(0, 3).map(
        (i, idx) => L.polyline(
            [[lat, lon], [bases[i].lat, bases[i].lon]],
            {color: timeToColor(preds[i])})
    )
    var labels = indices.slice(0, 3).map(
        function (i, idx) {
            var content = "<b>Regression:</b> " + preds[i].toFixed(2) + ' mins<br>'
                + '<b>Physical:</b> ' + physical_preds[i].toFixed(2) + ' mins<br>'
                + '<b>Nearest:</b> ' + ((knn_preds[i] !== 'no data') ? (knn_preds[i].toFixed(2) + ' mins') : knn_preds[i]);
            return L.popup()
                .setLatLng([bases[i].lat, bases[i].lon])
                .setContent(content)
        }
    )
    lines.map(l => l.addTo(predictionLayer))
    labels.map(l => l.addTo(predictionLayer))
    prevLines = lines
}

function onMapClick(e) {
    selectedPoint.setLatLng(e.latlng).addTo(predictionLayer)
    const latField = document.getElementById("latField")
    const lonField = document.getElementById("lonField")
    latField.value = e.latlng.lat
    lonField.value = e.latlng.lng
    updateMarkerPos(e.latlng.lat, e.latlng.lng)
}
map.on('click', onMapClick);

function onOkClick() {
    lat = document.getElementById("latField").value
    lon = document.getElementById("lonField").value
    selectedPoint.setLatLng([lat, lon]).addTo(predictionLayer)
    updateMarkerPos(lat, lon)
}

predictionLayer.addTo(map)


// helper functions
function physical_model(base, p_lat, p_lon) {
    //not using elevation as it didnt make big difference
    var b_lat = bases[base].lat;
    var b_lon = bases[base].lon;
    var dist = distance(b_lat, b_lon, p_lat, p_lon);
    // predefined speed and times
    var phase1_dist = 50*1/60;
    var phase3_dist =100*2/60;

    var phase2_dist = dist - phase1_dist - phase3_dist;

    
    if(phase2_dist <= 0) {
        return 60 * (dist/50);
    }

    var phase2_time = 60 * phase2_dist / 180
    // Time phase1 and phase3 = 3 min

    return phase2_time + 3
}

function predict(base, p_lat, p_lon) {
    var b_lat = bases[base].lat
    var b_lon = bases[base].lon
    var lat_delta = Math.abs(b_lat - p_lat);
    var lon_delta = Math.abs(b_lon - p_lon);
    var dist = distance(b_lat, b_lon, p_lat, p_lon);

    var intercept = 1.9671

    var pred = -1.53576875 * lat_delta + -0.84870441 * lon_delta + 0.32236224 * dist + bases[base].vehicleWeight + intercept
    return pred;
}

function distance(lat1, lon1, lat2, lon2) {
    if ((lat1 == lat2) && (lon1 == lon2)) {
        return 0;
    }
    else {
        var radlat1 = Math.PI * lat1/180;
        var radlat2 = Math.PI * lat2/180;
        var theta = lon1-lon2;
        var radtheta = Math.PI * theta/180;
        var dist = Math.sin(radlat1) * Math.sin(radlat2) + Math.cos(radlat1) * Math.cos(radlat2) * Math.cos(radtheta);
        if (dist > 1) {
            dist = 1;
        }
        dist = Math.acos(dist);
        dist = dist * 180/Math.PI;
        dist = dist * 60 * 1.1515;
        return dist * 1.609344;
    }
}



// --------------------------- HEAT MAP LAYER -------------------------------------------------

function drawRectAround(lat, lng, color, opacity, layer) {
    var x1 = lat - 0.025;
    var x2 = lat + 0.025;
    var y1 = lng - 0.025;
    var y2 = lng + 0.025;
    L.polygon([[x1,y1], [x1,y2], [x2, y2], [x2, y1]], {
        'color': color, 'opacity': 0.0, 'fillOpacity': opacity
    }).addTo(layer);
}

var colors =  {
    "Rega_1": "#00bfb2",
    "Rega_2": "#80B918",
    "Rega_3": "#e4ff1a",
    "Rega_4": "#00bfb2",
    "Rega_5": "#ff5714",
    "Rega_6": "#5f00ba",
    "Rega_7": "#CF1259",
    "Rega_8": "#1789fc",
    "Rega_9": "#484349",
    "Rega_10" : "#f7f0f0",

    "Rega_11" : "#00bfb2",
    "Rega_12" : "#80B918",
    "Rega_13" : "#e4ff1a",
    "Rega_14" : "#ffb800",
    "Rega_15" : "#ff5714",
    "Rega_16" : "#5f00ba",
    "Rega_17" : "#CF1259",
    "Rega_18" : "#1789fc",
    "Rega_19" : "#484349",
    "Rega_20" : "#f7f0f0"
}

function mode(arr){
    return arr.sort((a,b) =>
        arr.filter(v => v===a).length
            - arr.filter(v => v===b).length
    ).pop();
}

function mode2(arr){
    return arr.sort((a,b) =>
        arr.filter(v => v===a).length
            - arr.filter(v => v===b).length
    ).pop();
}

function drawServiceRegionLayer(lines) {
    lines.forEach(function(point) {
        var base = mode(point[2]);
        var color = "white";
        if ("undefined" === typeof(colors[base])) {
            // console.log("Undefined", base);
        } else {
            color = colors[base];
        }
        drawRectAround(point[0], point[1], color, 0.4, serviceRegionLayer);
    });
}

var gHeatMap = null;

function drawHeatmapLayer(heatmapData, which) {
    import('../libs/rainbowvis.js').then(function(obj) {
        // Add heatmap
        rainbow = new obj.Rainbow();
        if (which == 't1a') {
            rainbow.setNumberRange(0, 23.5);
        } else if (which == 't1c') {
            rainbow.setNumberRange(20, 40);
        } else if (which == 't1b') {
            rainbow.setNumberRange(0, 10);
        }
        rainbow.setSpectrum('green', 'yellow', 'red');
        heatmapData.forEach(function(point) {
            var avgTime = point[2];
            // console.log(avgTime);
            var color = "#" + rainbow.colourAt(avgTime);
            drawRectAround(point[0], point[1], color, 0.8, heatMapLayer);
        });

        // Add legend
        var legend = L.control({position: 'topright'});
        legend.onAdd = function (map) {
            var div = L.DomUtil.create('div', 'Average mission time');
            // div.innerHTML = "Average mission time"
            var minI = 0;
            var maxI = 0;
            if (which == 't1a') {
                minI = 0; maxI = 30;
            } else if (which == 't1c') {
                minI = 20; maxI = 45;
            }

            for (var i = minI; i < maxI; i+=5) {
                div.innerHTML += '<div style="background: ' + '#' + rainbow.colourAt(i) +
                    '; width: 15px; height: 15px;' + '"/>'
                //     div.innerHTML +=
                //     labels.push(
                //         '<i class="square" style="background:' + "red;" + '"></i> ' +
                //     (categories[i] ? categories[i] : '+'));
            }
            // div.innerHTML = div;//labels.join('FOO<br>');
            return div;
        };
        legend.addTo(map);
    });
}

function load_service_regions() {
    $.ajax({
        url: "data/service_regions.json",
        async: false,
        dataType: 'json',
        success: function(data) {
            drawServiceRegionLayer(data);
        }
    });
}

function load_heatmap(which) {
    $.ajax({
        url: "data/heatmap." + which + ".json",
        async: false,
        dataType: 'json',
        success: function(data) {
            drawHeatmapLayer(data, which);
        }
    });
}

function read_csv(input) {

    if(input.files && input.files[0]) {
        let reader = new FileReader();
        reader.readAsBinaryString(input.files[0]);
        reader.onload = function (e) {
            obj_csv.size = e.total;
            obj_csv.dataFile = e.target.result;
            csv_data = Papa.parse(obj_csv.dataFile).data;

            import ("./knn.js")
                .then(function(obj) {
                    // obj.save_heatmap(csv_data, 't1a');
                    // obj.save_heatmap(csv_data, 't1b');
                    // obj.save_heatmap(csv_data, 't1c');
                    // var heatmap = obj.generate_heatmap(csv_data, 't1a');
                    // drawHeatmapLayer(heatmap);
                    // obj.save_service_regions(csv_data);
                    // obj.load_service_regions(drawServiceRegionLayer);
                    // var lines = obj.generate_service_regions(csv_data);
                });
        }
    }
}

load_service_regions();
load_heatmap('t1a');
