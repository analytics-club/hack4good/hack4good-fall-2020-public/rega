#!/usr/bin/env python3
import numpy as np
import pandas as pd
import matplotlib
import matplotlib.pyplot as plt
import matplotlib.patches as mpatches
plt.style.use('ggplot')

import geopy.distance
import geopandas
import tilemapbase

import warnings
warnings.filterwarnings("ignore", category=matplotlib.cbook.mplDeprecation)

import location

from missions import add_times_patient_from_crew


def plot_avg_velocity(missions, which='t1.a'):  
    # t1.a: until patient radius; t1.b: from patient radius until landing on site
    RESOLUTION = 30
    all_diffs = np.zeros((RESOLUTION,))
    total_cnt = 0

    for i, mission in missions[0:1000].iterrows():
        print(i)
        # mission = missions.iloc[i]
        print(mission['time_patient'])
        #filter_positions_for_mission(mission)
        positions = location.filter_positions_for_mission(mission, which='all')

        if which == 't1.a':
            positions = positions[positions['gpstime'] <= mission['liftoff_time'] + mission['time_patient']]
        elif which == 't1.b':
            positions = positions[positions['gpstime'] >= mission['liftoff_time'] + mission['time_patient']]
            positions = positions[positions['gpstime'] <= mission['Ankunft_Einsatzort']]

        if len(positions) <= 1:
            continue
        diffs = location.get_location_differences(mission, positions)
        #     print(mission['liftoff_time'], mission['Ankunft_Einsatzort'], mission['Abschlusszeit'])
        #     print(location.filter_positions_for_mission(mission)[['gpstime', 'latitude', 'longitude']])
        for j in range(RESOLUTION):
            k = len(diffs) / RESOLUTION * j
            # print(int(np.floor(k)), len(diffs))
            all_diffs[j] += diffs[int(np.floor(k))]
        total_cnt += 1

    plt.plot(range(RESOLUTION), all_diffs / total_cnt * 80)
    plt.ylabel("Velocity km/h")
    plt.xlabel(f"Time (1/{RESOLUTION} parts)")
    plt.title(f"Average speed for {which}")
    plt.savefig(f"maps/speed-profile-{which}.png")
    plt.show()

    # x km / 45 seconds -> x km / 1 hour
    # 45 s * Y = 1 h -> Y = 1 h / 45 s = 60 * 60 s / 45 s
    # Y = 3600 / 45 s = 80


def create_time_histograms(bins=50):
    from main import missions_with_patient_arrival_positions
    missions = missions_with_patient_arrival_positions()
    missions = missions[missions['time_patient'] < pd.Timedelta(1, 'h')]
    fig, (ax1, ax2) = plt.subplots(nrows=1, ncols=2)
    time_until_radius = missions['time_patient'] / pd.Timedelta(1, 'm')
    time_until_landing = missions['Ankunft_Einsatzort'] - (missions['liftoff_time'] + missions['time_patient'])
    time_until_landing = time_until_landing / pd.Timedelta(1, 'm')
    time_until_landing = time_until_landing[time_until_landing.abs() < 60]
    ax1.hist(time_until_radius, bins=bins, density=True)
    ax1.set_xlabel("Minutes until in radius")
    ax2.hist(time_until_landing, bins=bins, density=True)
    ax2.set_xlabel("Minutes in radius until pilot set landing")
    plt.savefig("maps/t1-hist.png")
    plt.show()


def get_point_nearby(positions, landing_point, threshold_km=2, outside=False):
    # pos = location.filter_positions_for_mission(mission, which='all')
    for idx, row in positions.iterrows():
        lat, lng = row['latitude'], row['longitude']
        if (geopy.distance.distance(landing_point, (lat, lng)).km < threshold_km) != outside:
            return (idx, row['gpstime'], lat, lng)
    return None


def get_arrival_at_patient(mission, positions):
    landing_point = (mission['WGS84_Latitude'], mission['WGS84_Longitude'])
    return get_point_nearby(positions, landing_point)


def get_departure_at_patient(mission, positions, arrival_time):
    landing_point = (mission['WGS84_Latitude'], mission['WGS84_Longitude'])
    positions = positions[positions['gpstime'] > arrival_time]
    return get_point_nearby(positions, landing_point, outside=True)


def plot_trajectory(mission, i=None):

    # global missions
    # missions = missions[missions['time_patient'] > pd.Timedelta('1800s')]

    tilemapbase.init(create=True)
    t = tilemapbase.tiles.build_OSM()

    fig, ax = plt.subplots(figsize=(10,10))
    ax.xaxis.set_visible(False)
    ax.yaxis.set_visible(False)

    # mission = missions.iloc[i]
    # pos_all = location.filter_positions_for_mission(mission, which='all')

    pos = location.filter_positions_for_mission(mission, which='all')
    pos = geopandas.GeoDataFrame(pos, geometry=geopandas.points_from_xy(pos.latitude, pos.longitude))
    pos = pos.set_crs(epsg=4326)

    x1, y1 = pos['latitude'].min(), pos['longitude'].min()
    x2, y2 = pos['latitude'].max(), pos['longitude'].max()
    extent = tilemapbase.Extent.from_lonlat(y1 - 0.2, y2 + 0.2, x1 - 0.2, x2 + 0.2)
    extent = extent.to_aspect(1.0)

    plotter = tilemapbase.Plotter(extent, t, width=600)
    plotter.plot(ax, t)

    pos_all = pos
    pos = pos.to_crs({"init": "EPSG:3857"})
    x, y = location.project_mercator(pos['latitude'], pos['longitude'])
    ax.plot(x, y, color='orange', linewidth=2, zorder=10)
    
    pos = location.filter_positions_for_mission(mission, which='t1+')
    pos = geopandas.GeoDataFrame(pos, geometry=geopandas.points_from_xy(pos.latitude, pos.longitude))
    pos = pos.set_crs(epsg=4326)

    pos_ = pos

    x1, y1 = pos['latitude'].min(), pos['longitude'].min()
    x2, y2 = pos['latitude'].max(), pos['longitude'].max()

    pos = pos.to_crs({"init": "EPSG:3857"})
    x, y = location.project_mercator(pos['latitude'], pos['longitude'])
    ax.plot(x, y, color='blue', linewidth=2, zorder=11)


    pos = location.filter_positions_for_mission(mission, which='t1')
    pos = geopandas.GeoDataFrame(pos, geometry=geopandas.points_from_xy(pos.latitude, pos.longitude))
    pos = pos.set_crs(epsg=4326)

    x1, y1 = pos['latitude'].min(), pos['longitude'].min()
    x2, y2 = pos['latitude'].max(), pos['longitude'].max()

    pos = pos.to_crs({"init": "EPSG:3857"})
    x, y = location.project_mercator(pos['latitude'], pos['longitude'])
    ax.plot(x, y, color='green', linewidth=2, zorder=12)


    mission_x, mission_y = location.project_mercator(mission['WGS84_Latitude'], mission['WGS84_Longitude'])
    ax.scatter(mission_x, mission_y, color='red', linewidth=2, zorder=900)

    mission_x, mission_y = location.project_mercator(mission['Zielort_WGS84_Latitude'], mission['Zielort_WGS84_Longitude'])
    ax.scatter(mission_x, mission_y, marker='X', color='yellow', linewidth=5)

    landing_patient = get_arrival_at_patient(mission, pos_all)
    if landing_patient is not None:
        (idx, gpstime, latitude, longitude) = landing_patient
        x, y = location.project_mercator(latitude, longitude)
        ax.scatter(x, y, color='black', linewidth=2, zorder=1000)

        to_patient = get_departure_at_patient(mission, pos_all, gpstime)
        if to_patient is not None:
            (idx, gpstime, latitude, longitude) = to_patient
            x, y = location.project_mercator(latitude, longitude)
            ax.scatter(x, y, color='black', linewidth=2, zorder=1000)

    diffs = location.get_location_differences(mission=None, mission_positions=pos_)
    diffs.insert(0, 0)
    pos_['diffs'] = diffs

    landed_pos = pos_[pos_['diffs'] < 0.5]
    pred_lat, pred_lng = landed_pos['latitude'], landed_pos['longitude']

    pred_x, pred_y = location.project_mercator(pred_lat, pred_lng)
    ax.scatter(pred_x, pred_y, color='pink', linewidth=1, zorder=999)

    red_patch = mpatches.Patch(color='red', label='Patient location')
    pink_patch = mpatches.Patch(color='pink', label='Predicted landings (from speed)')
    green_patch = mpatches.Patch(color='green', label='Path to patient site (set by crew)')
    blue_patch = mpatches.Patch(color='blue', label='Path at patient site (according to crew)')
    orange_patch = mpatches.Patch(color='orange', label='Path after patient site (according to crew)')
    yellow_patch = mpatches.Patch(color='yellow', label='Destination (hospital)')
    black_patch = mpatches.Patch(color='black', label="Estimated arrival / departure patient")

    plt.legend(handles=[red_patch, pink_patch, green_patch, blue_patch, orange_patch, yellow_patch, black_patch])
    
    if i is not None:
        plt.savefig("trajectories-long/%d.png" % i)
    else:
        plt.show()



def visualize_maps():
    import geopandas
    import tilemapbase
    from tqdm.auto import tqdm
    missions = pd.read_csv("missions_cached.csv")
    missions['Abflug_Einsatzort'] = pd.to_datetime(missions['Abflug_Einsatzort'])
    missions['Ankunft_Einsatzort'] = pd.to_datetime(missions['Ankunft_Einsatzort'])
    missions['Abschlusszeit'] = pd.to_datetime(missions['Abschlusszeit'])
    missions['liftoff_time'] = pd.to_datetime(missions['liftoff_time'])
    missions = add_times_patient_from_crew(missions)
    missions = missions[missions['time_patient'] < np.timedelta64(6, 'h')]
    print("Before:", missions.shape)
    # TODO: 
    # if plotting until arrival time, use nonnegative time_patient
    # if plotting all time, use nonnegative total time
    missions = missions[(missions['Abflug_Einsatzort'] - missions['liftoff_time']) > pd.Timedelta('-1us')]
    # missions = missions[missions['time_patient'] > pd.Timedelta('-1us')]
    # missions = missions[missions['Abschlusszeit'] > missions['liftoff_time']]
    print("After:", missions.shape)
    missions = missions[missions['time_patient'] > pd.Timedelta('1800s')]
    missions = missions.sample(n=100)
    for idx, mission in tqdm(missions.iterrows()):
        plot_trajectory(mission, idx)

def visualize_velocity():
    from main import missions_with_patient_arrival_positions
    missions = missions_with_patient_arrival_positions()
    plot_avg_velocity(missions)


def extract_flight_features(missions, show_results=False):
    distance_ratios = []
    all_ratios = []
    ok, thrown = 0, 0

    for idx, mission in missions.iterrows():
        
        positions = location.filter_positions_for_mission(mission, which='all')
        landing_patient = get_arrival_at_patient(mission, positions)
        if landing_patient is None:
            all_ratios.append(None)
            continue
        (idx, gpstime, patient_lat, patient_lng) = landing_patient

        # calculate total distance in all line segments
        total_km = 0.
        first_position = None
        prev_position = None
        for j, pos in positions[positions['gpstime'] <= gpstime].iterrows():
            if prev_position is None:
                first_position = prev_position = pos
            else:
                last_coords = (prev_position['latitude'], prev_position['longitude'])
                pos_coords = (pos['latitude'], pos['longitude'])
                dist = geopy.distance.distance(last_coords, pos_coords)
                total_km += dist.km
                prev_position = pos
        
        # calculate air distance
        takeoff_lat, takeoff_lng = first_position['latitude'], first_position['longitude']
        straight_km = geopy.distance.distance((takeoff_lat, takeoff_lng), (patient_lat, patient_lng)).km
        
        # compare distances
        dist_ratio = total_km / (straight_km + 0.000001)
        if dist_ratio > 2 or dist_ratio < 0.9:
            # plot_trajectory(mission)
            thrown += 1
            all_ratios.append(None)
            continue
        ok += 1
        all_ratios.append(dist_ratio)
        distance_ratios.append(dist_ratio)

    # TODO: calculate overall direction
    # TODO: calculate wind speed and direction
    
    print(f"Distance ratio max: {np.max(distance_ratios)} min: {np.min(distance_ratios)} mean: {np.mean(distance_ratios)}")
    print(f"OK: {ok} Thrown away (too long): {thrown}")
    if show_results:
        plt.hist(distance_ratios, bins=100)
        plt.title(f"Distance ratio ({thrown} outliers removed)")
        plt.xlabel("Flight distance / straight line distance [km]")
        plt.ylabel("Mission count")
        plt.show()  # TODO: maybe throw zeros too?

    missions['distance_ratio'] = all_ratios
    return missions


def plot_missions_by_time(missions):

    fig, ax = plt.subplots(nrows=1, ncols=3, figsize=(15, 5))

    per_month_counts = []
    for month in range(12):
        m = missions[missions['liftoff_time'].dt.month == (month + 1)]
        per_month_counts.append(m.shape[0])
    ax[0].bar(range(1,13), per_month_counts)
    ax[0].set_xticks(range(1,13))
    ax[0].set_xlabel("Missions per month")

    per_hour_counts = []
    for hour in range(24):
        h = missions[missions['liftoff_time'].dt.hour == hour]
        per_hour_counts.append(h.shape[0])
    ax[1].bar(range(24), per_hour_counts)
    ax[1].set_xlabel("Missions per hour")

    per_day_counts = []
    for day in range(7):
        d = missions[missions['liftoff_time'].dt.weekday == day]
        per_day_counts.append(d.shape[0])
    ax[2].bar(range(7), per_day_counts)
    ax[2].set_xticks(range(7))
    ax[2].set_xticklabels(["Mo", "Tu", "We", "Th", "Fr", "Sa", "Su"])
    ax[2].set_xlabel("Missions per weekday")

    # per_month_counts = missions[missions['liftoff_time'].dt.month]
    # plt.hist(per_month_counts, bins=12)
    plt.show()


# visualize_velocity()
# create_time_histograms()
if __name__ == "__main__":
    from main import missions_with_patient_arrival_positions
    missions = missions_with_patient_arrival_positions()
    # extract_flight_features(missions, show_results=True)
    # plot_missions_by_time(missions)
    # print(missions.columns)
    # print(missions['Einsatzmittel'].unique())
    # print(missions[missions['Einsatzmittel'] == 'Rega_15'])
    # mins = missions['time_on_site'].values / pd.Timedelta(1, 'm')
    mins = missions['time_until_landing'].values / pd.Timedelta(1, 'm')
    plt.hist(mins, bins=50)
    plt.show()
    # print(missions['time_until_landing'])
